# App architecture

Architecture based on a View Model pattern.  
I designed this application using mostly Android Architecture Components (Room, Coroutines, Navigation, Data binding) and the following third-party libraries :

- Retrofit (REST client)
- Glide (Image loading and caching)
- Koin (Dependency injection) // Lighter and easier to setup for small projects compared to Dagger

## Presentation

All the views are based on a ConstraintLayout in order to best handle different devices size.  
Data binding to display the data.  
ViewModel are used to keep the state of views.  
Observer on livedata to ensure views are updated as soon as the database is updated.  

### Activities

1 activity as entry point holding a bottom navigation bar to navigate between the fragments.

### Fragments

4 fragments

#### Movies/Series Fragments

2 fragments diplaying each a list of movies and a list of series.  
Usage of nested recyclerviews to display items horizontally under categories.

#### Details Fragments

2 fragments to show details of movies/series.   
Opening from item in list.

#### Search Fragment

Diplay a grid of movies.  
Search view in the action bar allowing to search movies by name.  
3 choicechips, single choice to filter grid by popuplar/top rated/upcoming.

## Business

### Models

The models are used for the deserialization and database table structure and joins.

- Movies
- Series
- Page
- Categories

Many to Many relationship between Movies/Series and Categories

### Repositories

Repositories are interfaces between viewmodels and data layer (local & remote)

- MovieReposiory
- SerieRepository

## Peristence

### Room Database

Room database as cache.  
All data displayed in the views come from the database.

- AppDatabse
- Converters

### Data Access Objects

Pattern to request data from the database

- MovieDao
- SerieDao

## Webservices REST client

Usage of Retrofit in order to fetch remote data from TMDb API.  
Kotlin serialization and retrofit kotlin converter to serialize API responses to models.  
Interceptor used to add TMDb API key for every network calls

- WebService
- MoviesSeriesService
- Serializer
- ApiKeyInterceptor


# Questions

## Unique responsibility priinciple

Each class should have a unique function in order to avoid duplicating code.   
Duplicated code is more difficult to maintain because if there is one change, it has to be done on every class providing the same function.

## Clean code

In my opinion, a clean code is a code that any developer can read, understand and maintain without needing help from the developer who has written that code.