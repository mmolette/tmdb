package com.molette.moviesseriesdb.ui.utils

import android.content.Context

class Utils {

    companion object{

        fun calculateNoOfColumns(
            context: Context,
            columnWidthDpResource: Int
        ): Int { // For example columnWidthdp=180
            var columnWidth = context.resources.getDimension(columnWidthDpResource) / context.resources.getDisplayMetrics().density
            val displayMetrics = context.resources.displayMetrics
            val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
            return (screenWidthDp / columnWidth + 0.5).toInt()
        }
    }
}