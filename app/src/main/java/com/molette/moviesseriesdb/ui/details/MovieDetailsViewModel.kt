package com.molette.moviesseriesdb.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.repository.MovieRepository

class MovieDetailsViewModel(val movieRepository: MovieRepository) : ViewModel() {

    val id: MutableLiveData<Long> = MutableLiveData()

    val movie : LiveData<Movie> = Transformations.switchMap(id){
        movieRepository.getMovie(it)
    }
}
