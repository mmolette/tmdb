package com.molette.moviesseriesdb.ui.movies.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.molette.Seriesseriesdb.ui.Series.adapters.SerieAdapter
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.databinding.CategoryCellBinding

class SerieCategoryAdapter(val navController: NavController): RecyclerView.Adapter<SerieCategoryViewHolder>() {

    var data = mapOf<EntertainmentCategoryEnum, List<Serie>>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerieCategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val binding = DataBindingUtil.inflate<CategoryCellBinding>(layoutInflater, R.layout.category_cell, parent, false)
        binding.recyclerView.setRecycledViewPool(viewPool)

        return SerieCategoryViewHolder(binding, parent.context, navController)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: SerieCategoryViewHolder, position: Int) {
        var key = data.keys.toList().get(position)
        holder.bind(key, data.get(key)!!)
    }
}

class SerieCategoryViewHolder(
    val binding: CategoryCellBinding,
    val context: Context,
    val navController: NavController
): RecyclerView.ViewHolder(binding.root) {
    fun bind(category: EntertainmentCategoryEnum, series: List<Serie>){
        binding.categoryLabel = context.getString(category.ressourceId)

        val adapter = SerieAdapter(navController)
        binding.recyclerView.adapter = adapter
        adapter.data = series
    }
}