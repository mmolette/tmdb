package com.molette.moviesseriesdb.ui.search

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.ChipGroup
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.databinding.FragmentSearchBinding
import com.molette.moviesseriesdb.ui.search.adapter.SearchAdapter
import com.molette.moviesseriesdb.ui.utils.Utils
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchFragment : Fragment(), SearchView.OnQueryTextListener, ChipGroup.OnCheckedChangeListener {

    private val searchViewModel by viewModel<SearchViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentSearchBinding>(inflater, R.layout.fragment_search, container, false)
        val searchAdapter = SearchAdapter(findNavController())
        binding.searchRv.adapter = searchAdapter

        var nbColumn = Utils.calculateNoOfColumns(activity!!, R.dimen.item_width)
        binding.searchRv.layoutManager = GridLayoutManager(activity, nbColumn)

        binding.typeChipGroup.setOnCheckedChangeListener(this)

        var checkedChip = getCheckedChip()
        if (checkedChip != -1){
            binding.typeChipGroup.check(checkedChip)
        }

        searchViewModel.movies.observe(this, Observer {
            searchAdapter.data = it
        })

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.search)
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE)

        var searchView: android.widget.SearchView? = null
        if(searchItem != null){
            searchView = searchItem.actionView as android.widget.SearchView
        }
        if (searchView != null){
            searchView.isIconifiedByDefault = false
            searchView.setQuery(searchViewModel.filterText.value, false)
            searchView.setOnQueryTextListener(this)
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {

        searchViewModel.filterText.value = newText
        searchViewModel.filterChanged.value = true
        return true
    }

    private fun getCheckedChip(): Int{
        return when(searchViewModel.category.value){
            EntertainmentCategoryEnum.UPCOMING -> R.id.chip_upcoming
            EntertainmentCategoryEnum.TOP_RATED -> R.id.chip_top_rated
            EntertainmentCategoryEnum.POPULAR -> R.id.chip_popular
            else -> -1
        }
    }

    override fun onCheckedChanged(group: ChipGroup?, chipId: Int) {
        searchViewModel.category.value = when(chipId){
            R.id.chip_popular -> EntertainmentCategoryEnum.POPULAR
            R.id.chip_top_rated -> EntertainmentCategoryEnum.TOP_RATED
            R.id.chip_upcoming -> EntertainmentCategoryEnum.UPCOMING
            else -> EntertainmentCategoryEnum.NONE
        }
        searchViewModel.filterChanged.value = true
    }
}