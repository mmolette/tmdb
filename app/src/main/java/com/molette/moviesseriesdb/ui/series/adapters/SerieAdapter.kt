package com.molette.Seriesseriesdb.ui.Series.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.molette.moviesseriesdb.BuildConfig
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.databinding.CategoryItemCellBinding
import com.molette.moviesseriesdb.ui.movies.MoviesFragmentDirections
import com.molette.moviesseriesdb.ui.series.SeriesFragmentDirections


class SerieAdapter(val navController: NavController): RecyclerView.Adapter<SerieViewHolder>(){

    var data = listOf<Serie>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<CategoryItemCellBinding>(inflater, R.layout.category_item_cell, parent, false)
        
        return SerieViewHolder(binding, parent.context, navController)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: SerieViewHolder, position: Int) {
        holder.bind(data.get(position))
    }
}

class SerieViewHolder(
    val binding: CategoryItemCellBinding,
    val context: Context,
    val navController: NavController
) : RecyclerView.ViewHolder(binding.root){
    fun bind(serie: Serie){
        Glide.with(context)
            .load(BuildConfig.TMDB_IMAGE_BASE + serie.posterPath)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.posterItem)

        binding.posterItem.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                var directions = SeriesFragmentDirections.actionNavigationSerieToNavigationSerieDetails(id = serie.id)
                navController.navigate(directions)
            }
        })
    }
}