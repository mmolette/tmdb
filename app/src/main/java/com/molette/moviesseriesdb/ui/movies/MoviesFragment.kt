package com.molette.moviesseriesdb.ui.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.databinding.FragmentMoviesBinding
import com.molette.moviesseriesdb.ui.movies.adapters.MovieCategoryAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesFragment : Fragment() {

    private val moviesViewModel by viewModel<MoviesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentMoviesBinding>(inflater, R.layout.fragment_movies, container, false)

        val movieCategoryAdapter = MovieCategoryAdapter(findNavController())
        binding.moviesRv.adapter = movieCategoryAdapter

        moviesViewModel.categories.observe(this, Observer {
            movieCategoryAdapter.data = it
        })

        return binding.root
    }

}