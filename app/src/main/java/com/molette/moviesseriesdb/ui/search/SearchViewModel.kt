package com.molette.moviesseriesdb.ui.search

import androidx.lifecycle.*
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.repository.MovieRepository

class SearchViewModel(val movieRepository: MovieRepository) : ViewModel() {

    var filterText = MutableLiveData<String>("")

    var category = MutableLiveData<EntertainmentCategoryEnum>(EntertainmentCategoryEnum.NONE)

    val filterChanged = MutableLiveData<Boolean>(true)

    val movies = Transformations.switchMap(filterChanged){
        filter(filterText, category)
    }

    private fun filter(filter: LiveData<String>, category: LiveData<EntertainmentCategoryEnum>): LiveData<List<Movie>>{
        return when(category.value){
            EntertainmentCategoryEnum.NONE -> {
                if (filter.value.isNullOrBlank()){
                    movieRepository.all
                }else movieRepository.getMoviesByName(filter.value!!)
            }
            else -> {
                if (filter.value.isNullOrBlank()){
                    movieRepository.getMoviesByCategory(category.value!!)
                }else movieRepository.getMoviesByCategoryAndName(category.value!!, filter.value!!)
            }
        }
    }

}