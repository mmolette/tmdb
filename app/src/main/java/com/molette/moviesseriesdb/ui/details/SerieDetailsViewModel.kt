package com.molette.moviesseriesdb.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.business.repository.SerieRepository

class SerieDetailsViewModel(val serieRepository: SerieRepository) : ViewModel() {

    val id: MutableLiveData<Long> = MutableLiveData()

    val serie : LiveData<Serie> = Transformations.switchMap(id){
        serieRepository.getSerie(it)
    }
}
