package com.molette.moviesseriesdb.ui.movies

import android.util.Log
import androidx.lifecycle.*
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.repository.MovieRepository
import com.molette.moviesseriesdb.business.repository.SerieRepository
import kotlinx.coroutines.*
import java.lang.Exception

class MoviesViewModel(val movieRepository: MovieRepository) : ViewModel() {

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)
    val categories: MediatorLiveData<Map<EntertainmentCategoryEnum, List<Movie>>> = MediatorLiveData()

    init {
        Log.i("MoviesViewModel", "MoviesViewModel init")

        categories.addSource(movieRepository.popular){
            categories.value = combineMovies(movieRepository.popular, movieRepository.topRated, movieRepository.upcoming)
        }

        categories.addSource(movieRepository.topRated){
            categories.value = combineMovies(movieRepository.popular, movieRepository.topRated, movieRepository.upcoming)
        }

        categories.addSource(movieRepository.upcoming){
            categories.value = combineMovies(movieRepository.popular, movieRepository.topRated, movieRepository.upcoming)
        }

        getMovies()
    }

    private fun combineMovies(popular: LiveData<List<Movie>>, topRated: LiveData<List<Movie>>, upcoming: LiveData<List<Movie>>):Map<EntertainmentCategoryEnum, List<Movie>> {
        var map: MutableMap<EntertainmentCategoryEnum, List<Movie>> = mutableMapOf()

        if (!popular.value.isNullOrEmpty()){
            map.put(EntertainmentCategoryEnum.POPULAR, popular.value!!)
        }

        if (!topRated.value.isNullOrEmpty()){
            map.put(EntertainmentCategoryEnum.TOP_RATED, topRated.value!!)
        }

        if (!upcoming.value.isNullOrEmpty()){
            map.put(EntertainmentCategoryEnum.UPCOMING, upcoming.value!!)
        }

        return map
    }


    fun getMovies() {

        viewModelScope.launch {
            try {

                movieRepository.getPopularRemote()
                movieRepository.getTopRatedRemote()
                movieRepository.getUpcomingRemote()

            }catch (e: Exception){
                Log.e("getMovies", Log.getStackTraceString(e))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        viewModelJob.cancel()
        Log.i("MoviesViewModel", "MoviesViewModel cleared")
    }
}