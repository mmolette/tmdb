package com.molette.moviesseriesdb.ui.movies.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.databinding.CategoryCellBinding

class MovieCategoryAdapter(val navController: NavController): RecyclerView.Adapter<MovieCategoryViewHolder>() {

    var data = mapOf<EntertainmentCategoryEnum, List<Movie>>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieCategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val binding = DataBindingUtil.inflate<CategoryCellBinding>(layoutInflater, R.layout.category_cell, parent, false)
        binding.recyclerView.setRecycledViewPool(viewPool)

        return MovieCategoryViewHolder(binding, parent.context, navController)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MovieCategoryViewHolder, position: Int) {
        var key = data.keys.toList().get(position)
        holder.bind(key, data.get(key)!!)
    }
}

class MovieCategoryViewHolder(
    val binding: CategoryCellBinding,
    val context: Context,
    val navController: NavController
): RecyclerView.ViewHolder(binding.root) {
    fun bind(category: EntertainmentCategoryEnum, movies: List<Movie>){
        binding.categoryLabel = context.getString(category.ressourceId)

        val adapter = MovieAdapter(navController)
        binding.recyclerView.adapter = adapter
        adapter.data = movies
    }
}