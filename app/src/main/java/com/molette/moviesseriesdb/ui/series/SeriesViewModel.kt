package com.molette.moviesseriesdb.ui.series

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.repository.SerieRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.lang.Exception

class SeriesViewModel(val serieRepository: SerieRepository) : ViewModel() {

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)
    val categories: MediatorLiveData<Map<EntertainmentCategoryEnum, List<Serie>>> = MediatorLiveData()

    init {
        Log.i("SeriesViewModel", "SeriesViewModel init")

        categories.addSource(serieRepository.popular){
            categories.value = combineSeries(serieRepository.popular, serieRepository.topRated)
        }

        categories.addSource(serieRepository.topRated){
            categories.value = combineSeries(serieRepository.popular, serieRepository.topRated)
        }

        getSeries()
    }

    private fun combineSeries(popular: LiveData<List<Serie>>, topRated: LiveData<List<Serie>>):Map<EntertainmentCategoryEnum, List<Serie>> {
        var map: MutableMap<EntertainmentCategoryEnum, List<Serie>> = mutableMapOf()

        if (!popular.value.isNullOrEmpty()){
            map.put(EntertainmentCategoryEnum.POPULAR, popular.value!!)
        }

        if (!topRated.value.isNullOrEmpty()){
            map.put(EntertainmentCategoryEnum.TOP_RATED, topRated.value!!)
        }

        return map
    }


    fun getSeries() {

        viewModelScope.launch {
            try {

                serieRepository.getPopularRemote()
                serieRepository.getTopRatedRemote()

            }catch (e: Exception){
                Log.e("getSeries", Log.getStackTraceString(e))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        viewModelJob.cancel()
        Log.i("SeriesViewModel", "SeriesViewModel cleared")
    }
}