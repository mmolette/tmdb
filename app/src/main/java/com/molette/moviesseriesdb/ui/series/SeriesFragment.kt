package com.molette.moviesseriesdb.ui.series

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.databinding.FragmentSeriesBinding
import com.molette.moviesseriesdb.ui.movies.adapters.SerieCategoryAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SeriesFragment : Fragment() {

    private val seriesViewModel by viewModel<SeriesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentSeriesBinding>(inflater, R.layout.fragment_series, container, false)

        val movieCategoryAdapter = SerieCategoryAdapter(findNavController())
        binding.seriesRv.adapter = movieCategoryAdapter

        seriesViewModel.categories.observe(this, Observer {
            movieCategoryAdapter.data = it
        })

        return binding.root
    }
}