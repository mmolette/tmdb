package com.molette.moviesseriesdb.ui

import android.app.Application
import com.molette.moviesseriesdb.business.di.app_module
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            modules(app_module)
        }
    }
}