package com.molette.moviesseriesdb.ui.details

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.molette.moviesseriesdb.BuildConfig

import com.molette.moviesseriesdb.R
import com.molette.moviesseriesdb.databinding.SerieDetailsFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SerieDetailsFragment : Fragment() {

    companion object {

    }

    private val viewModelMovie by viewModel<SerieDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<SerieDetailsFragmentBinding>(inflater, R.layout.serie_details_fragment, container, false)

        viewModelMovie.serie.observe(this, Observer {
            binding.serie = it

            Glide.with(activity!!)
                .load(BuildConfig.TMDB_BACKDROP_BASE + it.backdropPath)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.imageView)
        })
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val args: MovieDetailsFragmentArgs by navArgs()
        viewModelMovie.id.value = args.id
    }

}
