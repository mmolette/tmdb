package com.molette.moviesseriesdb.business.repository

import androidx.lifecycle.LiveData
import com.molette.moviesseriesdb.business.model.Movie

interface EntertainmentRepository<T> {

    val all: LiveData<List<T>>
    val popular: LiveData<List<T>>
    val topRated: LiveData<List<T>>


}