package com.molette.moviesseriesdb.business.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey
    val id: EntertainmentCategoryEnum
) {
}