package com.molette.moviesseriesdb.business.repository

import androidx.lifecycle.LiveData
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum

interface MovieRepository: EntertainmentRepository<Movie> {

    val upcoming: LiveData<List<Movie>>

    // Remote
    suspend fun getPopularRemote()
    suspend fun getTopRatedRemote()
    suspend fun getUpcomingRemote()

    // Local
    fun getMoviesByName(name: String): LiveData<List<Movie>>
    fun getMoviesByCategoryAndName(category: EntertainmentCategoryEnum, name: String): LiveData<List<Movie>>
    fun getMoviesByCategory(category: EntertainmentCategoryEnum): LiveData<List<Movie>>
    fun getMovie(id: Long): LiveData<Movie>
}