package com.molette.moviesseriesdb.business.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.molette.moviesseriesdb.business.api.serializer.DateSerializer
import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Entity(tableName = "movies")
@Serializable
data class Movie(

    @PrimaryKey
    @ColumnInfo(name = "movie_id")
    val id: Long = 0L,

    @ColumnInfo(name = "movie_popularity")
    val popularity: Float = 0f,

    @SerialName("vote_count")
    @ColumnInfo(name = "movie_vote_count")
    val voteCount: Int = 0,

    @SerialName("poster_path")
    @ColumnInfo(name = "movie_poster_ath")
    val posterPath: String? = "",

    @SerialName("backdrop_path")
    @ColumnInfo(name = "movie_backdrop_path")
    val backdropPath: String? = "",

    @SerialName("original_language")
    @ColumnInfo(name = "movie_original_language")
    val originalLanguage: String = "",

    @SerialName("genre_ids")
    @ColumnInfo(name = "movie_genre_ids")
    val genreIds: List<Int> = listOf(),

    @SerialName("vote_average")
    @ColumnInfo(name = "movie_vote_average")
    val voteAverage: Float = 0f,

    @ColumnInfo(name = "movie_overview")
    val overview: String = "",

    @ColumnInfo(name = "movie_video")
    val video: Boolean = false,

    @ColumnInfo(name = "movie_adult")
    val adult: Boolean = false,

    @SerialName("original_title")
    @ColumnInfo(name = "movie_original_title")
    val originalTitle: String = "",

    @ColumnInfo(name = "movie_title")
    val title: String = "",

    @SerialName("release_date")
    @ColumnInfo(name = "movie_release_date")
    @Serializable(with = DateSerializer::class)
    val releaseDate: Date = Date()

) {

}