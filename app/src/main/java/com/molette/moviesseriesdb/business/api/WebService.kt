package com.molette.moviesseriesdb.business.api

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.molette.moviesseriesdb.BuildConfig
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class WebService {

    companion object {
        private val contentType = MediaType.get("application/json")

        private val retrofit = Retrofit.Builder()
            .client(createOkHttpClient())
            .baseUrl(BuildConfig.TMDB_BASE)
            .addConverterFactory(
                Json(
                JsonConfiguration(
                    strictMode = false,
                    useArrayPolymorphism = true,
                    encodeDefaults = false
                )
            ).asConverterFactory(contentType))
            .build()

        fun createMoviesSeriesService(): MoviesSeriesService {

            return retrofit.create(MoviesSeriesService::class.java)
        }

        private fun createOkHttpClient(): OkHttpClient {

            val builder = OkHttpClient.Builder()

            // Authenticator
            builder.addInterceptor(MoviesSeriesService.apiInterceptor)

            //timeOuts
            builder
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)

            builder.retryOnConnectionFailure(true)

            return builder.build()
        }
    }

}