package com.molette.moviesseriesdb.business.repository.impl

import android.util.Log
import androidx.lifecycle.LiveData
import com.molette.moviesseriesdb.business.api.MoviesSeriesService
import com.molette.moviesseriesdb.business.db.AppDatabase
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.business.db.join.SerieCategoryJoin
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.repository.SerieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class SerieRepositoryImpl(
    val remoteDataSource: MoviesSeriesService,
    val localDataSource: AppDatabase
): SerieRepository {

    override val all: LiveData<List<Serie>> = localDataSource.serieDao.getAll()
    override val popular: LiveData<List<Serie>> = localDataSource.serieDao.getSeriesForCategory(EntertainmentCategoryEnum.POPULAR)
    override val topRated: LiveData<List<Serie>> = localDataSource.serieDao.getSeriesForCategory(EntertainmentCategoryEnum.TOP_RATED)

    override fun getSerie(id: Long): LiveData<Serie> {
        return localDataSource.serieDao.getSerie(id)
    }

    override suspend fun getPopularRemote() {

        try {
            withContext(Dispatchers.IO){

                val series = remoteDataSource.getPopularSeries(1).results

                val inserted = localDataSource.serieDao.insertAll(series)
                Log.d("getPopularSeries", "$inserted inserted series")

                // Create join between category and movie
                series.forEach {
                    var join = SerieCategoryJoin(
                        EntertainmentCategoryEnum.POPULAR.categoryId,
                        it.id
                    )
                    localDataSource.serieDao.insert(join)
                }
            }
        }catch (e:Exception){
            Log.e("getPopularRemote", Log.getStackTraceString(e))
        }

    }

    override suspend fun getTopRatedRemote() {
        withContext(Dispatchers.IO){

            val series = remoteDataSource.getTopRatedSeries(1).results

            val inserted = localDataSource.serieDao.insertAll(series)
            Log.d("getPopularSeries", "$inserted inserted series")

            // Create join between category and movie
            series.forEach {
                var join = SerieCategoryJoin(
                    EntertainmentCategoryEnum.TOP_RATED.categoryId,
                    it.id
                )
                localDataSource.serieDao.insert(join)
            }
        }
    }
}