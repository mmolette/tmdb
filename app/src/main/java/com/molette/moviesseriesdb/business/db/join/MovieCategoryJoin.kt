package com.molette.moviesseriesdb.business.db.join

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.molette.moviesseriesdb.business.model.Category
import com.molette.moviesseriesdb.business.model.Movie

@Entity(primaryKeys = arrayOf("movie_id", "category_id"),
    indices = arrayOf(
        Index(value = arrayOf("movie_id")),
        Index(value = arrayOf("category_id"))
    ),
    foreignKeys = arrayOf(
        ForeignKey(entity = Movie::class,
            parentColumns = arrayOf("movie_id"),
            childColumns = arrayOf("movie_id")),
        ForeignKey(entity = Category::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("category_id")))
    )
data class MovieCategoryJoin(
    @ColumnInfo(name = "category_id") val categoryId: Long = 0L,
    @ColumnInfo(name = "movie_id") val movieId: Long = 0L
) {
}