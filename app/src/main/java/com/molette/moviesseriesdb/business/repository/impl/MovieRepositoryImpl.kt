package com.molette.moviesseriesdb.business.repository.impl

import android.util.Log
import androidx.lifecycle.LiveData
import com.molette.moviesseriesdb.business.api.MoviesSeriesService
import com.molette.moviesseriesdb.business.db.AppDatabase
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.db.join.MovieCategoryJoin
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class MovieRepositoryImpl(
    val remoteDataSource: MoviesSeriesService,
    val localDataSource: AppDatabase
): MovieRepository {

    override val all: LiveData<List<Movie>> = localDataSource.movieDao.getAll()
    override val popular: LiveData<List<Movie>> = localDataSource.movieDao.getMoviesForCategory(EntertainmentCategoryEnum.POPULAR)
    override val topRated: LiveData<List<Movie>> = localDataSource.movieDao.getMoviesForCategory(EntertainmentCategoryEnum.TOP_RATED)
    override val upcoming: LiveData<List<Movie>> = localDataSource.movieDao.getMoviesForCategory(EntertainmentCategoryEnum.UPCOMING)

    override fun getMovie(id: Long): LiveData<Movie> {
        return localDataSource.movieDao.getMovie(id)
    }

    override fun getMoviesByName(name: String): LiveData<List<Movie>> {
        Log.e("getMoviesByName", name)
        var movies = localDataSource.movieDao.getMoviesByName("%$name%")
        return movies
    }

    override fun getMoviesByCategoryAndName(
        category: EntertainmentCategoryEnum,
        name: String
    ): LiveData<List<Movie>> {
        return localDataSource.movieDao.getMoviesByCategoryAndName(category, "%$name%")
    }

    override fun getMoviesByCategory(category: EntertainmentCategoryEnum): LiveData<List<Movie>> {
        return localDataSource.movieDao.getMoviesForCategory(category)
    }

    override suspend fun getPopularRemote() {

        withContext(Dispatchers.IO){

            try {
                val movies = remoteDataSource.getPopularMovies(1).results
                val inserted = localDataSource.movieDao.insertAll(movies)
                Log.d("getPopularMovies", "$inserted inserted series")

                // Create join between category and movie
                movies.forEach {
                    var join = MovieCategoryJoin(
                        EntertainmentCategoryEnum.POPULAR.categoryId,
                        it.id
                    )
                    localDataSource.movieDao.insert(join)
                }
            }catch (e: Exception){
                Log.e("getPopularRemote", Log.getStackTraceString(e))
            }

        }
    }

    override suspend fun getTopRatedRemote() {
        withContext(Dispatchers.IO){

            try {
                val movies = remoteDataSource.getTopRatedMovies(1).results

                val inserted = localDataSource.movieDao.insertAll(movies)
                Log.d("getTopRatedMovies", "$inserted inserted series")

                // Create join between category and movie
                movies.forEach {
                    var join = MovieCategoryJoin(
                        EntertainmentCategoryEnum.TOP_RATED.categoryId,
                        it.id
                    )
                    localDataSource.movieDao.insert(join)
                }
            }catch (e: Exception){
                Log.e("getTopRatedRemote", Log.getStackTraceString(e))
            }
        }
    }

    override suspend fun getUpcomingRemote() {
        withContext(Dispatchers.IO){

            try {
                val movies = remoteDataSource.getUpcomingMovies(1).results

                val inserted = localDataSource.movieDao.insertAll(movies)
                Log.d("getUpcomingMovies", "$inserted inserted series")

                // Create join between category and movie
                movies.forEach {
                    var join = MovieCategoryJoin(
                        EntertainmentCategoryEnum.UPCOMING.categoryId,
                        it.id
                    )
                    localDataSource.movieDao.insert(join)
                }
            }catch (e:Exception){
                Log.e("getUpcomingRemote", Log.getStackTraceString(e))
            }

        }
    }
}