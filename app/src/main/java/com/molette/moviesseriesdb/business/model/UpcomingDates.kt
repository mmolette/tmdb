package com.molette.moviesseriesdb.business.model

import com.molette.moviesseriesdb.business.api.serializer.DateSerializer
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
class UpcomingDates(
    @Serializable(with = DateSerializer::class)
    val maximum: Date,
    @Serializable(with = DateSerializer::class)
    val minimum: Date
){

}