package com.molette.moviesseriesdb.business.model.enums

import com.molette.moviesseriesdb.R

enum class EntertainmentCategoryEnum(val categoryId: Long, val ressourceId: Int) {
    POPULAR(1, R.string.category_popular),
    TOP_RATED(2, R.string.category_top_rated),
    UPCOMING(3, R.string.category_upcoming),
    NONE(0, R.string.category_none);
}

fun Long.toEntertainmentCategoryEnum() = when (this) {
    EntertainmentCategoryEnum.POPULAR.categoryId -> EntertainmentCategoryEnum.POPULAR
    EntertainmentCategoryEnum.TOP_RATED.categoryId -> EntertainmentCategoryEnum.TOP_RATED
    EntertainmentCategoryEnum.UPCOMING.categoryId -> EntertainmentCategoryEnum.UPCOMING
    else -> EntertainmentCategoryEnum.NONE
}