package com.molette.moviesseriesdb.business.db.join

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.molette.moviesseriesdb.business.model.Category
import com.molette.moviesseriesdb.business.model.Serie

@Entity(primaryKeys = arrayOf("serie_id", "category_id"),
    indices = arrayOf(
        Index(value = arrayOf("serie_id")),
        Index(value = arrayOf("category_id"))
    ),
    foreignKeys = arrayOf(
        ForeignKey(entity = Serie::class,
            parentColumns = arrayOf("serie_id"),
            childColumns = arrayOf("serie_id")),
        ForeignKey(entity = Category::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("category_id"))
    )
)
data class SerieCategoryJoin(
    @ColumnInfo(name = "category_id") val categoryId: Long = 0L,
    @ColumnInfo(name = "serie_id") val SerieId: Long = 0L
) {
}