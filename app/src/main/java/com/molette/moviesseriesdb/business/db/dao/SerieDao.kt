package com.molette.moviesseriesdb.business.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.molette.moviesseriesdb.business.model.Serie
import com.molette.moviesseriesdb.business.db.join.SerieCategoryJoin
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum

@Dao
interface SerieDao: BaseDao<Serie> {

    @Query("SELECT * FROM series")
    fun getAll(): LiveData<List<Serie>>

    @Query("SELECT * FROM series WHERE serie_id = :id")
    fun getSerie(id: Long): LiveData<Serie>

    @Query("SELECT * FROM series INNER JOIN seriecategoryjoin ON series.serie_id = seriecategoryjoin.serie_id WHERE seriecategoryjoin.category_id = :category")
    fun getSeriesForCategory(category: EntertainmentCategoryEnum): LiveData<List<Serie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(join: SerieCategoryJoin)

}