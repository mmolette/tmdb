package com.molette.moviesseriesdb.business.di

import android.widget.SearchView
import androidx.room.Room
import com.molette.moviesseriesdb.business.api.WebService
import com.molette.moviesseriesdb.business.db.AppDatabase
import com.molette.moviesseriesdb.business.db.dao.MovieDao
import com.molette.moviesseriesdb.business.repository.MovieRepository
import com.molette.moviesseriesdb.business.repository.SerieRepository
import com.molette.moviesseriesdb.business.repository.impl.MovieRepositoryImpl
import com.molette.moviesseriesdb.business.repository.impl.SerieRepositoryImpl
import com.molette.moviesseriesdb.ui.details.MovieDetailsViewModel
import com.molette.moviesseriesdb.ui.details.SerieDetailsViewModel
import com.molette.moviesseriesdb.ui.movies.MoviesViewModel
import com.molette.moviesseriesdb.ui.search.SearchViewModel
import com.molette.moviesseriesdb.ui.series.SeriesViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

var app_module = module {

    // API
    factory { WebService.createMoviesSeriesService() }

    // Room database
    single {
        AppDatabase.buildDb(androidContext())
    }

    factory { get<AppDatabase>().movieDao }
    factory { get<AppDatabase>().serieDao }

    // Repositories
    factory<MovieRepository> { MovieRepositoryImpl(get(), get())  }
    factory<SerieRepository> { SerieRepositoryImpl(get(), get())  }

    // ViewModels
    viewModel { MoviesViewModel(get()) }
    viewModel { SeriesViewModel(get()) }
    viewModel { SearchViewModel(get()) }
    viewModel { MovieDetailsViewModel(get()) }
    viewModel { SerieDetailsViewModel(get()) }
}