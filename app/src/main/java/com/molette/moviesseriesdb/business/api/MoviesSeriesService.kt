package com.molette.moviesseriesdb.business.api

import com.molette.moviesseriesdb.business.model.*
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface MoviesSeriesService {

    companion object{
        val apiInterceptor = ApiKeyInterceptor()

        const val PAGE_PARAM = "page"
    }

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("$PAGE_PARAM") page: Int = 1
    ): Page<Movie>

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("$PAGE_PARAM") page: Int = 1
    ): Page<Movie>

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(
        @Query("$PAGE_PARAM") page: Int = 1
    ): Page<Movie>

    @GET("tv/popular")
    suspend fun getPopularSeries(
        @Query("$PAGE_PARAM") page: Int = 1
    ): Page<Serie>

    @GET("tv/top_rated")
    suspend fun getTopRatedSeries(
        @Query("$PAGE_PARAM") page: Int = 1
    ): Page<Serie>

}