package com.molette.moviesseriesdb.business.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.molette.moviesseriesdb.business.db.converter.*
import com.molette.moviesseriesdb.business.db.dao.MovieDao
import com.molette.moviesseriesdb.business.db.dao.SerieDao
import com.molette.moviesseriesdb.business.model.*
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.db.join.MovieCategoryJoin
import com.molette.moviesseriesdb.business.db.join.SerieCategoryJoin
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.serialization.ImplicitReflectionSerializer

@UseExperimental(ImplicitReflectionSerializer::class)
@TypeConverters(DateTypeConverter::class,
    EntertainmentEnumConverter::class,
    IntListTypeConverter::class,
    StringListTypeConverter::class)
@Database(entities = [Movie::class, Serie::class, MovieCategoryJoin::class, SerieCategoryJoin::class, Category::class],
    version = 1, exportSchema = false)
abstract class AppDatabase(): RoomDatabase() {

    abstract val serieDao: SerieDao
    abstract val movieDao: MovieDao

    companion object {

        const val DATABASE_NAME = "movies_series_db"

        fun buildDb(context: Context): AppDatabase{
            return Room.databaseBuilder(context,
                AppDatabase::class.java,
                DATABASE_NAME)
                .addCallback(object : Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)

                        GlobalScope.async {

                            try {
                                for (category in EntertainmentCategoryEnum.values()){
                                    var contentValues = ContentValues()
                                    contentValues.put("id", category.categoryId)
                                    db.insert("categories", SQLiteDatabase.CONFLICT_REPLACE, contentValues)
                                }
                            }catch (e: Exception){
                                Log.e("Appdateabse.buildDb", e.printStackTrace().toString())
                            }

                        }
                    }
                }).build()
        }
    }

}