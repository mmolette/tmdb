package com.molette.moviesseriesdb.business.db.converter

import androidx.room.TypeConverter
import com.molette.moviesseriesdb.business.model.Movie
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.parse
import kotlinx.serialization.parseList
import kotlinx.serialization.stringify

@ImplicitReflectionSerializer
class StringListTypeConverter {

    @TypeConverter
    fun toStringList(value: String): List<String> = Json.parseList(value)

    @TypeConverter
    fun toString(value: List<String>): String = Json.stringify(value)
}

@ImplicitReflectionSerializer
class IntListTypeConverter {

    @TypeConverter
    fun toIntList(value: String): List<Int> = Json.parseList(value)

    @TypeConverter
    fun toString(value: List<Int>): String = Json.stringify(value)

}