package com.molette.moviesseriesdb.business.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.molette.moviesseriesdb.business.model.Movie
import com.molette.moviesseriesdb.business.db.join.MovieCategoryJoin
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum

@Dao
interface MovieDao: BaseDao<Movie> {

    @Query("SELECT * FROM movies")
    fun getAll(): LiveData<List<Movie>>

    @Query("SELECT * FROM movies WHERE movie_id = :id")
    fun getMovie(id: Long): LiveData<Movie>

    @Transaction
    @Query("SELECT * FROM movies INNER JOIN moviecategoryjoin ON movies.movie_id = moviecategoryjoin.movie_id WHERE moviecategoryjoin.category_id = :category")
    fun getMoviesForCategory(category: EntertainmentCategoryEnum): LiveData<List<Movie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(join: MovieCategoryJoin)

    @Transaction
    @Query("SELECT * FROM movies WHERE movie_title LIKE :name ORDER BY movie_title")
    fun getMoviesByName(name: String): LiveData<List<Movie>>

    @Transaction
    @Query("SELECT * FROM movies INNER JOIN moviecategoryjoin ON movies.movie_id = moviecategoryjoin.movie_id WHERE moviecategoryjoin.category_id = :category AND movie_title LIKE :name ORDER BY movie_title")
    fun getMoviesByCategoryAndName(category: EntertainmentCategoryEnum, name: String): LiveData<List<Movie>>

}