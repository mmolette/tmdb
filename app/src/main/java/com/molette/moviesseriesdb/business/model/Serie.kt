package com.molette.moviesseriesdb.business.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.molette.moviesseriesdb.business.api.serializer.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Entity(tableName = "series")
@Serializable
data class Serie(

    @PrimaryKey
    @ColumnInfo(name = "serie_id")
    val id: Long = 0L,

    @SerialName("original_name")
    @ColumnInfo(name = "serie_original_name")
    val originalName: String = "",

    @SerialName("genre_ids")
    @ColumnInfo(name = "serie_genre_ids")
    val genreIds: List<Int> = listOf(),

    @ColumnInfo(name = "serie_name")
    val name: String = "",

    @ColumnInfo(name = "serie_popularity")
    val popularity: Float = 0f,

    @SerialName("origin_country")
    @ColumnInfo(name = "serie_origin_country")
    val originCountry: List<String> = listOf(),

    @SerialName("vote_count")
    @ColumnInfo(name = "serie_vote_count")
    val voteCount: Int = 0,

    @SerialName("first_air_date")
    @ColumnInfo(name = "serie_first_air_date")
    @Serializable(with = DateSerializer::class)
    val firstAirDate: Date = Date(),

    @SerialName("backdrop_path")
    @ColumnInfo(name = "serie_backdrop_path")
    val backdropPath: String? = "",

    @SerialName("original_language")
    @ColumnInfo(name = "serie_original_language")
    val originalLanguage: String = "",

    @SerialName("vote_average")
    @ColumnInfo(name = "serie_vote_average")
    val voteAverage: Float = 0f,

    @ColumnInfo(name = "serie_overview")
    val overview: String = "",

    @SerialName("poster_path")
    @ColumnInfo(name = "serie_poster_ath")
    val posterPath: String? = ""

) {


}