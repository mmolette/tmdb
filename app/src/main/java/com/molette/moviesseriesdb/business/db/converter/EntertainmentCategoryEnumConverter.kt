package com.molette.moviesseriesdb.business.db.converter

import androidx.room.TypeConverter
import com.molette.moviesseriesdb.business.model.enums.EntertainmentCategoryEnum
import com.molette.moviesseriesdb.business.model.enums.toEntertainmentCategoryEnum

class EntertainmentEnumConverter {

    @TypeConverter
    fun toLong(value: EntertainmentCategoryEnum) = value.categoryId

    @TypeConverter
    fun toEnum(value: Long) = value.toEntertainmentCategoryEnum()
}