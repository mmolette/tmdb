package com.molette.moviesseriesdb.business.repository

import androidx.lifecycle.LiveData
import com.molette.moviesseriesdb.business.model.Serie

interface SerieRepository: EntertainmentRepository<Serie> {

    suspend fun getPopularRemote()
    suspend fun getTopRatedRemote()

    fun getSerie(id: Long): LiveData<Serie>
}