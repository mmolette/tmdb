package com.molette.moviesseriesdb.business.api.serializer

import android.util.Log
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Serializer(forClass = Date::class)
object DateSerializer : KSerializer<Date> {
    override val descriptor: SerialDescriptor =
        StringDescriptor.withName("DateSerializer")

    override fun serialize(output: Encoder, obj: Date) {
        output.encodeString(obj.time.toString())
    }

    override fun deserialize(input: Decoder): Date {
        val dateStr = input.decodeString()
        var df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).runCatching { parse(dateStr) }.getOrNull().also {
            if(it == null){
                Log.e("DateSerializer","$dateStr is not compatible with $df")
            }
        } ?: Date(0L)
    }
}