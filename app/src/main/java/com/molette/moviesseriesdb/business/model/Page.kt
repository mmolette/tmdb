package com.molette.moviesseriesdb.business.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class Page<T>(
    @SerialName("page")
    val page: Int = 0,
    @SerialName("total_results")
    val totalResults: Int = 0,
    @SerialName("total_pages")
    val totalPages: Int = 0,
    @SerialName("dates")
    val dates: UpcomingDates? = null,
    @SerialName("results")
    val results: List<T> = listOf()
) {

}