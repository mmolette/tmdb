package com.molette.moviesseriesdb.business.db.converter

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class DateTypeConverter {

    @TypeConverter
    fun toDate(value: String?): Date? = if (value.isNullOrBlank()) null else SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(value)

    @TypeConverter
    fun toString(value: Date?): String? = if (value == null) null else SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(value)
}